﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using WebApp.DTO;
using WebApp.DAL;
using WebApp.DAL.Models;

namespace WebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<User>> Register([FromForm] UserDTO model)
        {
            if (!ModelState.IsValid) return null;
            var user = new User { Email = model.Email, UserName = model.Email, Name = model.Name };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                await _userManager.AddToRoleAsync(user, "reader");
                return Ok(user);
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromForm] UserDTO model)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);
            if (result.Succeeded)
                return Ok(model.Name);
            ModelState.AddModelError("", "Неправильный логин или пароль");
            return BadRequest();
        }

        [HttpPost]
        [Route("edit")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> EditRoles(string email)
        {
            User user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                await _userManager.AddToRoleAsync(user, "initiator");
                return Ok();
            }
            return BadRequest();
        }

        [HttpPost]
        [Route("logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }

        //private async Task<ClaimsIdentity> GetIdentityAsync(string email, string password)
        //{
        //    User user = await _userManager.FindByEmailAsync(email);
        //    var passwordChecker = new PasswordHasher<User>().VerifyHashedPassword(user, user.PasswordHash, password);
        //    if (passwordChecker == PasswordVerificationResult.Success)
        //    {
        //        var claims = new List<Claim>
        //        {
        //            new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email)
        //        };
        //        ClaimsIdentity claimsIdentity =
        //        new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
        //            ClaimsIdentity.DefaultRoleClaimType);
        //        return claimsIdentity;
        //    }

        //    return null;
        //}
    }
}
