﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebApp.DTO;
using WebApp.DAL;
using WebApp.DAL.Models;
using WebApp.DAL.Interfaces;
using WebApp.DAL.MaterialsStuff;

namespace WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MaterialController : ControllerBase
    {
        private readonly IMaterialStuff _materialStuff;
        private readonly ILogger<MaterialController> _logger;

        public MaterialController(IMaterialStuff materialStuff, ILogger<MaterialController> logger)
        {
            _materialStuff = materialStuff;
            _logger = logger;
        }

        // GET: api/Material/{id}
        [HttpGet("id")]
        public IActionResult GetMaterial(int id)
        {
            var material = _materialStuff.GetMaterial(id);
            if (material == null)
                return BadRequest();
            _logger.LogInformation("Get material with id " + id);
            return Ok(material);
        }

        // POST: api/Material
        [HttpPost]
        [Authorize(Roles = "admin, initiator")]
        public IActionResult AddMaterial([FromForm] MaterialDTO material)
        {
            if (!(material.Name != null && material.File != null && material.File.Length < 2147483648))
                return BadRequest();
            Material newMaterial = new Material { Name = material.Name, Category = material.Category };
            var answer = _materialStuff.AddMaterial(newMaterial, material.File);
            if (answer != null)
            {
                _logger.LogInformation("Add material with name " + material.Name + " and Category" + material.Category);
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("download")]
        public IActionResult DownloadMaterial(string name, int? version)
        {
            var answer = _materialStuff.DownloadMaterial(name, version);
            if (answer == null)
                return BadRequest();
            _logger.LogInformation("Download material with name " + name + " version " + version);
            return File(answer, "application/octet-stream", name);
        }

        [HttpGet]
        public ActionResult<IList<Material>> ListMaterials()
        {
            _logger.LogInformation("List all materials.");
            return Ok(_materialStuff.ListMaterials());
        }

        // POST: api/Material/change
        [HttpPost]
        [Route("change")]
        [Authorize(Roles = "admin, initiator")]
        public IActionResult ChangeMaterial([FromForm] MaterialDTO material)
        {
            if (material.Name == null || material.File == null)
                return BadRequest();
            var answer = _materialStuff.ChangeMaterial(material.Name, material.File);
            if (answer != null)
            {
                _logger.LogInformation("Upgrade material " + material.Name);
                return Ok();
            }
            return BadRequest();
        }

    }
}
