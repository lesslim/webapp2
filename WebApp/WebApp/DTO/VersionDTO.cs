﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.DAL.Models;

namespace WebApp.DTO
{
    public class VersionDTO
    {
        public string Name { get; set; }
        public IFormFile File { get; set; }
        public Category Category { get; set; }
    }
}
