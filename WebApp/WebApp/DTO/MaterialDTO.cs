﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.DAL.Models;

namespace WebApp.DTO
{
    public class MaterialDTO
    {
        public string Name { get; set; }
        public Category Category { get; set; }
        public IFormFile File { get; set; }
    }
}
