﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Material = WebApp.DAL.Models.Material;
using Version = WebApp.DAL.Models.Version;

namespace WebApp.DAL.Interfaces
{
    public interface IMaterialStuff
    {
        Version AddMaterial(Material material, IFormFile file);
        Material GetMaterial(int Id);
        byte[] DownloadMaterial(string name, int? version);
        IList<Material> ListMaterials();
        Version ChangeMaterial(string name, IFormFile file);
    }
}
