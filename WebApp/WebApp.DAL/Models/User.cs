﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.DAL.Models
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
    }
}
