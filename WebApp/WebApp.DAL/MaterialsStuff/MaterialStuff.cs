﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApp.DAL.Models;
using WebApp.DAL.Interfaces;
using Version = WebApp.DAL.Models.Version;

namespace WebApp.DAL.MaterialsStuff
{
    public class MaterialStuff : IMaterialStuff
    {
        private readonly ApplicationContext _context;
        private readonly IConfiguration _config;
        public MaterialStuff(ApplicationContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }


        public Version AddMaterial(Material material, IFormFile file)
        {
            string ext = file.FileName.Split(".").Last();
            Version newVersion;
            if (_context.Materials.FirstOrDefault(p => p.Name == material.Name) == null)
            {
                newVersion = new Version
                {
                    Material = material,
                    Path = _config.GetValue<string>("PathFiles") + material.Name + "_1" + $".{ext}",
                    Release = 1,
                    Size = file.Length,
                    UploadDateTime = DateTime.Now
                };
                using (var filestream = new FileStream(newVersion.Path, FileMode.Create))
                {
                    file.CopyTo(filestream);
                }
                _context.Materials.Add(material);
                _context.Versions.Add(newVersion);
                _context.SaveChanges();
                return newVersion;
            }
            return null;
        }

        public Material GetMaterial(int id)
        {
            var material = _context.Materials.Include(p => p.Versions).FirstOrDefault(p => p.Id == id);
            if (material == null)
                return null;
            return (Material)material;
        }

        public byte[] DownloadMaterial(string name, int? version)
        {
            string Path;
            byte[] mas;
            var material = _context.Materials.Include(p => p.Versions).FirstOrDefault(p => p.Name == name);
            if (material != null)
            {
                if (version != null)
                    Path = _config.GetValue<string>("PathFiles") + material.Name + "_" + version;
                else
                    Path = _config.GetValue<string>("PathFiles") + material.Name + "_" + material.Versions.Count();
                mas = System.IO.File.ReadAllBytes(Path);
                return mas;
            }
            return null;
        }

        public IList<Material> ListMaterials()
        {
            return _context.Materials.Include(p => p.Versions).ToList<Material>();
        }

        public Version ChangeMaterial(string name, IFormFile file)
        {
            Material material = _context.Materials.Include(p => p.Versions).FirstOrDefault(p => p.Name == name);
            if (material == null)
                return null;
            Version newVersion;
            newVersion = new Version
            {
                Material = material,
                Path = _config.GetValue<string>("PathFiles") + material.Name + "_" + (material.Versions.Count() + 1) + $".{file.FileName.Split(".").Last()}",
                Release = material.Versions.Count() + 1,
                Size = file.Length,
                UploadDateTime = DateTime.Now
            };
            using (var filestream = new FileStream(newVersion.Path, FileMode.Create))
            {
                file.CopyTo(filestream);
            }
            _context.Versions.Add(newVersion);
            _context.SaveChanges();
            return newVersion;
        }

    }
}
